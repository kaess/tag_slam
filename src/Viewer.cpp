/**
 * @file Viewer.cpp
 * @brief Visualize Apriltags and camera trajectory.
 * @author: Michael Kaess
 */

#include <vector>
#include <map>

#if defined(__APPLE__) && defined(__MACH__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif
#include <SDL.h>
#include <SDL_thread.h>

#include <isam/isam.h>

#include "Viewer.h"

using namespace std;
using namespace isam;
using namespace Eigen;

inline double rad_to_degrees(double rad) {
  return rad * 180 / M_PI;
}

// current window size, with fixed initial size
GLsizei width = 800;
GLsizei height = 600;

// set to true whenever new drawing primitives were added
// multi-thread access variable, use mutex
bool redisplay_requested = false;
// set to true if processing thread should exit
// multi-thread access variable, use mutex
bool exit_request = false;

// redraw only if something changed (only used by main thread)
bool redraw = false;

// background color (gray scale 0 to 1)
float bg_color = 0.0f;

std::vector<isam::Pose3d> _poses;
std::map<int32_t, isam::Pose3d> _targets;

Pose3d eye_;

GLuint gl_list;

//SDL_Thread* thread_;
SDL_mutex* mutex_;

static void drawTag(const Pose3d& pose) {
  glPushMatrix();
  glPushAttrib(GL_CURRENT_BIT);

  glTranslatef(pose.x(), pose.y(), pose.z());

  glRotatef(rad_to_degrees(pose.yaw()), 0., 0., 1.);
  glRotatef(rad_to_degrees(pose.pitch()), 0., 1., 0.);
  glRotatef(rad_to_degrees(pose.roll()), 1., 0., 0.);

  double size = 0.166;
  double h = size / 2.;

  glBegin(GL_QUADS);
#if 1
  double d = size / 8.;
  for (int i = 0; i < 8; i++) {
    double x = i * d - h;
    for (int j = 0; j < 8; j++) {
      if (i == 0 || i == 7 || j == 0 || j == 7 || (i % 2) == (j % 2)) {
        double y = j * d - h;
        glVertex3f(x, y, 0);
        glVertex3f(x, y + d, 0);
        glVertex3f(x + d, y + d, 0);
        glVertex3f(x + d, y, 0);
      }
    }
  }
#else
  glVertex3f(-h,-h,0);
  glVertex3f(-h, h,0);
  glVertex3f( h, h,0);
  glVertex3f( h,-h,0);
#endif
  glEnd();

  glPopAttrib();
  // todo: reset color?
  glPopMatrix();
}

GLUquadricObj* quadric_internal = nullptr;

GLUquadricObj* getQuadric() {
  if (!quadric_internal) {
    quadric_internal = gluNewQuadric();
  }
  return quadric_internal;
}

void drawTetra(const Pose3d& pose, double size, bool mark) {
  glPushMatrix();
  glTranslatef(pose.x(), pose.y(), pose.z());
  glRotatef(rad_to_degrees(pose.yaw()), 0., 0., 1.);
  glRotatef(rad_to_degrees(pose.pitch()), 0., 1., 0.);
  glRotatef(rad_to_degrees(pose.roll()), 1., 0., 0.);

  if (mark) {
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    gluSphere(getQuadric(), size * 1.5, 5, 5);
    glPopAttrib();
  }
  glBegin(GL_POLYGON);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  glBegin(GL_POLYGON);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, 0.0, size / 2.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  glBegin(GL_POLYGON);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, 0.0, size / 2.0);
  glEnd();
  glBegin(GL_POLYGON);
  glVertex3f(-size, 0.0, size / 2.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  // draw outline in black
  glPushAttrib(GL_CURRENT_BIT);
  glColor3f(0, 0, 0);
  glBegin(GL_LINE_LOOP);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  glBegin(GL_LINE_LOOP);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, 0.0, size / 2.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  glBegin(GL_LINE_LOOP);
  glVertex3f(size, 0.0, 0.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, 0.0, size / 2.0);
  glEnd();
  glBegin(GL_LINE_LOOP);
  glVertex3f(-size, 0.0, size / 2.0);
  glVertex3f(-size, size / 2.0, 0.0);
  glVertex3f(-size, -size / 2.0, 0.0);
  glEnd();
  glPopAttrib();
  glPopMatrix();
}

/**
 * Redraw the scene into a GLList.
 */
void populateGLList() {
  glNewList(gl_list, GL_COMPILE_AND_EXECUTE);
  for (auto pose : _poses) {
    double size = 0.2;
    bool mark = false;
    drawTetra(pose, size, mark);
  }
  for (auto target : _targets) {
    drawTag(target.second);
  }
  glEndList();
}

/**
 * Draw everything, using the GLList if no change in the actual content.
 */
void drawGL() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glClearColor(bg_color, bg_color, bg_color, 1.0f);
  glLoadIdentity();

  /* Camera rotation */
  glTranslated(-eye_.x(), -eye_.y(), -eye_.z());
  glRotated(rad_to_deg(eye_.roll()), 1.0f, 0.0f, 0.0f);
  glRotated(rad_to_deg(eye_.pitch()), 0.0f, 1.0f, 0.0f);
  glRotated(rad_to_deg(eye_.yaw()), 0.0f, 0.0f, 1.0f);

  GLfloat light_position0[] = { 1.0, 1.0, 1.0, 0.0 };
  glLightfv(GL_LIGHT0, GL_POSITION, light_position0);

  // redraw (re-populate the GL list) only if something changed
  SDL_LockMutex(mutex_);
  if (redisplay_requested) {
    redisplay_requested = false;
    populateGLList();
  }
  SDL_UnlockMutex(mutex_);

  // draws the list
  glCallList(gl_list);
}

/**
 * Initialize OpenGL (only done once at beginning.
 */
void initGL() {
  glShadeModel(GL_SMOOTH);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_COLOR_MATERIAL);
  //  glEnable(GL_CULL_FACE);
}

/**
 * Close graphics window and tell process thread to quit, then clean up.
 */
void quit() {
  SDL_QuitSubSystem(SDL_INIT_VIDEO);
  SDL_LockMutex(mutex_);
  exit_request = true;
  SDL_UnlockMutex(mutex_);
//  SDL_WaitThread(thread_, nullptr);
//  SDL_DestroyMutex(mutex_);
//  SDL_Quit();
}

/**
 * Resize the viewport, at creation, or after window was resized by user.
 */
void resize(GLsizei w, GLsizei h, int videoFlags) {
  if (w < 10) {
    w = 10;
  }
  if (h < 10) {
    h = 10;
  }
  width = w;
  height = h;

  if (SDL_SetVideoMode(w, h, 0, videoFlags) == nullptr) {
    require(false, "Could not open GL window");
  }

  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0, (float) w / (float) h, 0.1, 10000.0);
  glMatrixMode(GL_MODELVIEW);
}

/**
 * Provide scale factor to make translation and scaling more sensitive
 * if further away from origin to allow for fast navigation.
 */
double factor(bool use_sqrt) {
  double distance = eye_.trans().vector().norm();
  if (use_sqrt) {
    return 1. + sqrt(distance) / 2.;
  } else {
    return 1. + distance / 20.;

  }
}

/**
 * Incrementally change rotation of camera/scene.
 */
void rotate(int dx, int dy) {
  eye_.set_roll(eye_.roll() + deg_to_rad(dy));
  eye_.set_pitch(eye_.pitch() + deg_to_rad(dx));
  redraw = true;
}

void rotate2(int dx, int dy) {
  eye_.set_roll(eye_.roll() + deg_to_rad(dy));
  eye_.set_yaw(eye_.yaw() + deg_to_rad(dx));
  redraw = true;
}

/**
 * Incrementally change translation of camera/scene.
 */
void translate(int dx, int dy) {
  eye_.set_x(eye_.x() - dx * 0.03 * factor(false));
  eye_.set_y(eye_.y() + dy * 0.03 * factor(false));
  redraw = true;
}

/**
 * Incrementally change scale of scene.
 */
void scale(int dx, int dy) {
  eye_.set_z(eye_.z() + dy * 0.1 * factor(true));
  redraw = true;
}

/**
 * Reset viewpoint to default starting position.
 */
void reset() {
  eye_ = Pose3d(0.0f, 0.0f, 100.0f, 0.0f, 0.0f, 0.0f);
  redraw = true;
}

/**
 * Switch background between black and white (useful for screen shots).
 */
void toggleColor() {
  bg_color = 1.0f - bg_color;
}

/**
 * Process key press.
 */
void keyPress(SDL_keysym *keysym) {
  switch (keysym->sym) {
  case SDLK_ESCAPE:
  case SDLK_q:
    quit();
    break;
  case SDLK_r:
    reset();
    break;
  case SDLK_c:
    toggleColor();
    break;
  default:
    break;
  }
}

/**
 * Process mouse wheel for scaling function.
 */
void processMouseWheel(int button) {
  if (button == SDL_BUTTON_WHEELUP) {
    scale(0, -2. * factor(true));
  } else if (button == SDL_BUTTON_WHEELDOWN) {
    scale(0, 2. * factor(true));
  }
}

/**
 * Process mouse motion, allowing navigation with various
 * mouse key and modifier key combinations.
 */
void mouseMotion(int x, int y) {
  static int x_pos = 0;
  static int y_pos = 0;

  // ignore if mouse outside window - avoids strange behavior on Mac
  if (x == 0 || y == 0 || x == width - 1 || y == height - 1)
    return;

  int dx = x - x_pos;
  int dy = y - y_pos;
  SDLMod modState = SDL_GetModState();
  Uint8 mouseState = SDL_GetMouseState(nullptr, nullptr);

  if (mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)) {
    if (modState & KMOD_SHIFT) {
      scale(dx, dy);
    } else if (modState & KMOD_CTRL) {
      translate(dx, dy);
    } else if (modState & KMOD_ALT) {
      rotate2(dx, dy);
    } else {
      rotate(dx, dy);
    }
  } else if (mouseState & SDL_BUTTON(SDL_BUTTON_MIDDLE)) {
    translate(dx, dy);
  } else if (mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT)) {
    scale(dx, dy);
  }

  x_pos = x;
  y_pos = y;
}

/**
 * Setup SDL, only called once at startup.
 */
int setupSDL() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    require(false, "Could not initialize SDL");
  }

//  atexit(quit);

  // create a GL window
  int videoFlags = SDL_OPENGL | SDL_RESIZABLE | SDL_DOUBLEBUF;
  const SDL_VideoInfo* videoInfo = SDL_GetVideoInfo();
  if (videoInfo->hw_available) {
    videoFlags |= SDL_HWSURFACE;
  } else {
    videoFlags |= SDL_SWSURFACE;
  }
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  SDL_WM_SetCaption("3D Tags Viewer", "opengl");

  return videoFlags;
}

int mainLoop(void* unused) {
  int videoFlags = setupSDL();
  resize(width, height, videoFlags);
  initGL();

  // allocate space for an OpenGL list
  gl_list = glGenLists(1);

  // SDL even loop
  bool done = false;
  bool isActive = true;
  while (!done) {
    // save CPU cycles: only redraw scene if change in viewpoint etc
    redraw = false;
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_VIDEORESIZE:
        resize(event.resize.w, event.resize.h, videoFlags);
        initGL();
        break;
      case SDL_KEYDOWN:
        keyPress(&event.key.keysym);
        break;
      case SDL_MOUSEBUTTONDOWN:
        processMouseWheel(event.button.button);
        break;
      case SDL_MOUSEMOTION:
        mouseMotion(event.button.x, event.button.y);
        break;
      case SDL_ACTIVEEVENT:
        if ((event.active.gain == 0) && (event.active.state & SDL_APPACTIVE)) {
          isActive = false;
        } else {
          isActive = true;
          redraw = true;
        }
        break;
      case SDL_QUIT:
        done = true;
        break;
      }
    }

    // also redraw if scene itself changed;
    // note that redisplay_requested is reset in populateGLList
    SDL_LockMutex(mutex_);
    if (redisplay_requested) {
      redraw = true;
    }
    SDL_UnlockMutex(mutex_);

    if (isActive && redraw) {
      drawGL();
      SDL_GL_SwapBuffers();
    }

    SDL_Delay(20); // don't eat up all CPU cycles, restrict to 50Hz

  }

  quit();

  return 0;
}

void Viewer::run() {
  reset();

  mutex_ = SDL_CreateMutex();

  mainLoop(nullptr);
}

bool Viewer::exitRequested() {
  SDL_LockMutex(mutex_);
  bool ret = exit_request;
  SDL_UnlockMutex(mutex_);
  return ret;
}

Viewer::Viewer() {
}

void Viewer::update(const vector<Pose3d> poses,
    const map<int32_t, Pose3d> targets) {
  SDL_LockMutex(mutex_);
  _poses = poses;
  _targets = targets;
  redisplay_requested = true;
  SDL_UnlockMutex(mutex_);
}
