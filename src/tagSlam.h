/**
 * @file tagSlam.h
 * @brief Data structures and iSAM factor for Apriltags
 * @author: Michael Kaess
 */

#pragma once

#include <Eigen/Dense>

#include <isam/isam.h>

#include "Calibration.h"

// Apriltag detection: image coordinates of four corners in fixed order
class TagCorners {
public:
  isam::Point2d bl, br, tr, tl;

  TagCorners(isam::Point2d bl, isam::Point2d br, isam::Point2d tr,
      isam::Point2d tl) :
      bl(bl), br(br), tr(tr), tl(tl) {
  }

  friend std::ostream& operator<<(std::ostream& out, const TagCorners& t) {
    out << "(" << t.bl << " ; " << t.br << " ; " << t.tr << " ; " << t.tl
        << ")";
    return out;
  }

  Eigen::VectorXd vector() const {
    Eigen::VectorXd ret(8);
    ret << bl.vector(), br.vector(), tr.vector(), tl.vector();
    return ret;
  }
};

namespace isam {

class Pose3d_April_Factor: public FactorT<TagCorners> {
  Pose3d_Node* _camera;
  Pose3d_Node* _tag;
  Calibration _calibration;
  Eigen::Matrix4d _rot;

public:

  /**
   * Constructor.
   * @param camera The pose of the camera observing the tag.
   * @param tag The pose of the tag.
   * @param measure The measurements of the corners of the tag
   * @param noise The 8x8 noise matrix.
   * @param calibration The camera calibration parameters.
   */
  Pose3d_April_Factor(Pose3d_Node* camera, Pose3d_Node* tag,
      const TagCorners& measure, const Noise& noise, const Calibration& calib) :
      FactorT<TagCorners>("Pose3d_April_Factor", 8, noise, measure), _camera(
          camera), _tag(tag), _calibration(calib) {

    _nodes.resize(2);
    _nodes[0] = camera;
    _nodes[1] = tag;

    _rot.setZero();
    _rot(0, 1) = 1;
    _rot(1, 2) = 1;
    _rot(2, 0) = 1;
    _rot(3, 3) = 1;
  }

  void initialize() {
    require(_tag->initialized(),
        "Pose3d_April_Factor requires tag to be initialized");
    require(_camera->initialized(),
        "Pose3d_April_Factor requires camera to be initialized");
  }

  inline Point2d proj(const Eigen::Matrix<double, 3, 4>& P, double u,
      double v) const {
    Eigen::Vector4d corner(u, v, 0., 1.);
    Eigen::Vector3d x;
    x = P * corner;
    return Point2d(x(0) / x(2), x(1) / x(2));
  }

  Eigen::VectorXd basic_error(Selector s = ESTIMATE) const {
    const Pose3d& cam = _camera->value(s);
    const Pose3d& tag = _tag->value(s);
    Eigen::Matrix<double, 3, 4> P = _calibration.K() * _rot
        * tag.ominus(cam).wTo();

    double h = _calibration.tagSize / 2;
    TagCorners predicted(proj(P, -h, -h), proj(P, h, -h), proj(P, h, h),
        proj(P, -h, h));
    Eigen::VectorXd err = predicted.vector() - _measure.vector();
    return err;
  }

};

}
