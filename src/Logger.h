/*
 * @file Logger.h
 * @brief Read and write file with sequence of camera images.
 * @author: Michael Kaess
 */

#pragma once

#include "opencv2/opencv.hpp"

class Logger {
  FILE* _file;
  bool _write;
  int32_t _numFrames;
  int32_t _numFramesAvailable;
  // temporary buffers, only allocated once for efficiency
  std::vector<uint8_t> _imageCompressed;

public:

  Logger(const std::string& fname, bool doWrite = false);

  bool getFrame(cv::Mat& image, uint64_t& timestamp);

  void writeFrame(const cv::Mat& image, uint64_t timestamp);

  void close();
};
