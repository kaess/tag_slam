/**
 * @file main.cpp
 * @brief Simple monocular SLAM with Apriltags and iSAM
 * @author: Michael Kaess
 */

#include <string>

using namespace std;

const string usage =
    "\n"
        "Usage:\n"
        "  tagSlam [OPTION...] [INFILE]\n"
        "\n"
        "Options:\n"
        "  -h  -?          Show help options\n"
        "  -C <xxhxx>      Tag family (default 36h11)\n"
        "  -D <id>         Video device ID (if multiple cameras present)\n"
        "  -F <fx>         Focal length in pixels\n"
        "  -L <LOGFILE>    Write image stream to LOGFILE\n"
        "  -W <width>      Image width (default 640, availability depends on camera)\n"
        "  -H <height>     Image height (default 480, availability depends on camera)\n"
        "  -S <size>       Tag size (square black frame) in meters\n"
        "\n";

const string intro = "\n"
    "TagSLAM\n"
    "(C) 2014, Michael Kaess, Carnegie Mellon University\n"
    "\n";

#include <signal.h>
#include <thread>

#include <opencv2/opencv.hpp>

#include "tagSlam.h"
#include "Mapper.h"
#include "Viewer.h"
#include "Logger.h"
#include "Tags.h"

// getopt / command line options processing
#include <unistd.h>
extern int optind;
extern char *optarg;

bool __stop = false;

class Main {
  Mapper _mapper;
  Viewer _viewer;
  int _deviceNumber;

  Calibration _calib;
  int _width, _height;

  string _logName;
  bool _writeLog;

  string _tagCode;

  Logger* _logger;

public:

  Main() :
      _deviceNumber(-1), _calib(calib_example), _width(640), _height(480), _logName(), _writeLog(
          false), _tagCode("36h11"), _logger(nullptr) {
  }

  void init(int argc, char** argv) {
    int c;
    while ((c = getopt(argc, argv, ":h?C:D:F:H:L:S:W:")) != -1) {
      // Each option character has to be in the string in getopt();
      // the first colon changes the error character from '?' to ':';
      // a colon after an option means that there is an extra
      // parameter to this option; 'W' is a reserved character
      switch (c) {
      case 'h':
      case '?':
        cout << intro;
        cout << usage;
        exit(0);
        break;
      case 'C':
        _tagCode = optarg;
        break;
      case 'D':
        _deviceNumber = atoi(optarg);
        break;
      case 'F':
        _calib.fx = atof(optarg);
        _calib.fy = _calib.fx;
        break;
      case 'H':
        _height = atoi(optarg);
        _calib.py = _height / 2;
        break;
      case 'L':
        _logName = optarg;
        _writeLog = true;
        break;
      case 'S':
        _calib.tagSize = atof(optarg);
        break;
      case 'W':
        _width = atoi(optarg);
        _calib.px = _width / 2;
        break;
      case ':': // unknown option, from getopt
        cout << "ERROR: Unknown option" << endl;
        cout << intro;
        cout << usage;
        exit(1);
        break;
      }
    }

    if (argc == optind + 1) {
      _logName = argv[optind];
      _writeLog = false;
    }
  }

  void processFrame(cv::Mat& image, const uint64_t timestamp, Tags& tags) {
    // Apriltags
    detections_t detections = tags.detectTags(image);
    static int sum_detected = 0;
    sum_detected += detections.size();
    cout << endl << timestamp << " " << detections.size() << " features (sum "
        << sum_detected << ")" << endl;

    // visualize image
    tags.showFrame(image, timestamp, detections);

    // update map
    _mapper.add_frame(timestamp, detections, _calib);

    // update viewer
    if (!_viewer.exitRequested()) {
      _viewer.update(_mapper.poses(), _mapper.targets());
    }
  }

  void run() {
    thread visThread(&Viewer::run, _viewer);
    visThread.detach();

    Tags tags(_tagCode, _calib);

    if (!_logName.empty()) {
      _logger = new Logger(_logName, _writeLog);
    }

    cv::Mat image;

    if (_logger && !_writeLog) {

      // load from file
      uint64_t timestamp;
      while (!__stop) {
        if (_logger->getFrame(image, timestamp)) {
          processFrame(image, timestamp, tags);
        }
        if (cv::waitKey(5) != -1 || _viewer.exitRequested()) {
          __stop = true;
        }
      }

    } else {

      // connect to video device
      cout << "Setting up camera..." << endl;
      cv::VideoCapture cap(_deviceNumber);
      if (!cap.isOpened()) {
        cerr << "ERROR: Can't find video device " << _deviceNumber << "\n";
        return;
      }

      if (cap.get(CV_CAP_PROP_FRAME_WIDTH) != _width) {
        cap.set(CV_CAP_PROP_FRAME_WIDTH, _width);
        cap.set(CV_CAP_PROP_FRAME_HEIGHT, _height);
      }

      if (_width != cap.get(CV_CAP_PROP_FRAME_WIDTH)
          || _height != cap.get(CV_CAP_PROP_FRAME_HEIGHT)) {
        cout << "ERROR: Failed to set requested resolution" << endl;
        return;
      }

      cout << "Camera setup at " << _width << "x" << _height << endl;
      cout << "Calibration: " << _calib << endl;

      // get images from camera
      while (!__stop) {

        // retrieve image
        cap >> image;
        uint64_t timestamp = tic();
        if (_logger && _writeLog) {
          _logger->writeFrame(image, timestamp);
        }

        processFrame(image, timestamp, tags);

        if (cv::waitKey(5) != -1 || _viewer.exitRequested()) {
          __stop = true;
        }
      }

      cap.release();

    }

    if (_logger) {
      _logger->close();
      delete _logger;
    }

  }

};

// catch signals to allow clean shutdown of Logger
void terminate(int signum) {
  cout << "Shutting down, please wait..." << endl;
  __stop = true;
}

void setupSignalHandlers(void (*handler)(int)) {
  struct sigaction newAction, oldAction;
  memset(&newAction, 0, sizeof(newAction));
  newAction.sa_handler = handler;
  sigemptyset(&newAction.sa_mask);

  // Set termination handlers and preserve ignore flag.
  sigaction(SIGINT, NULL, &oldAction);
  if (oldAction.sa_handler != SIG_IGN)
    sigaction(SIGINT, &newAction, NULL);
  sigaction(SIGHUP, NULL, &oldAction);
  if (oldAction.sa_handler != SIG_IGN)
    sigaction(SIGHUP, &newAction, NULL);
  sigaction(SIGTERM, NULL, &oldAction);
  if (oldAction.sa_handler != SIG_IGN)
    sigaction(SIGTERM, &newAction, NULL);
}

int main(int argc, char** argv) {

  setupSignalHandlers(terminate);

  Main __main;
  __main.init(argc, argv);
  __main.run();

  return 0;
}
