/*
 * @file Logger.cpp
 * @brief Read and write file with sequence of camera images.
 * @author: Michael Kaess
 */

#include <zlib.h> // compress2

#include "opencv2/opencv.hpp"

#include "Logger.h"

using namespace std;

inline void check(int ret) {
  if (ret != 1) {
    cout << "ERROR: Problem reading from log file" << endl;
    exit(1);
  }
}

Logger::Logger(const string& fname, bool doWrite) {
  _numFrames = 0;
  _numFramesAvailable = 0;
  _write = doWrite;
  _imageCompressed.resize(640 * 480 * 3);

  if (_write) {

    _file = fopen(fname.c_str(), "wb+");
    // upon closing of log file, the number of frames will be written to this space holder
    int32_t zero = 0;
    fwrite(&zero, sizeof(int32_t), 1, _file);

  } else {

    _file = fopen(fname.c_str(), "rb");
    check(fread(&_numFramesAvailable, sizeof(int32_t), 1, _file));
    cout << "Log file contains " << _numFramesAvailable << " frames." << endl;

  }
}

bool Logger::getFrame(cv::Mat& image, uint64_t& timestamp) {
  if (_numFrames >= _numFramesAvailable) {

    // end of log
    return false;

  } else {

    _numFrames++;

    uint32_t imageCompressedSize;

    check(fread(&timestamp, sizeof(int64_t), 1, _file));
    check(fread(&imageCompressedSize, sizeof(int32_t), 1, _file));
    check(fread(&_imageCompressed[0], imageCompressedSize, 1, _file));

    // assumes image and depth have already been allocated
    cv::imdecode(_imageCompressed, CV_LOAD_IMAGE_COLOR, &image);

    return true;
  }
}

void Logger::writeFrame(const cv::Mat& image, uint64_t timestamp) {
  vector<int> jpegParams = { CV_IMWRITE_JPEG_QUALITY, 85, 0 };
  // note: _imageCompress is resized here
  cv::imencode(".jpg", image, _imageCompressed, jpegParams);
  int32_t imageSize = _imageCompressed.size();

  fwrite(&timestamp, sizeof(int64_t), 1, _file);
  fwrite(&imageSize, sizeof(int32_t), 1, _file);
  fwrite(&_imageCompressed[0], imageSize, 1, _file);
  fflush(_file);
  _numFrames++;
}

void Logger::close() {
  if (_write) {
    fseek(_file, 0, SEEK_SET);
    fwrite(&_numFrames, sizeof(int32_t), 1, _file);
    fflush(_file);
  }
  fclose(_file);
  _file = nullptr;
}
