# tagSlam Makefile
# Michael Kaess, 2014

# default parameters, including multi-core building
make = make -j 4 -C build --no-print-directory

# creates isam libraries and isam binary
all: build build/Makefile
	@$(make)

# internal target: the actual build directory
build:
	@mkdir -p build bin lib include

# internal target: populate the build directory
build/Makefile:
	cd build && cmake ..

# default target: any target such as "clean", "example"...
# is simply passed on to the cmake-generated Makefile 
%::
	@$(make) $@
