/**
 * @file Calibration.h
 * @brief Calibration parameters for Apriltags
 * @author: Michael Kaess
 */

#pragma once

#include <Eigen/Dense>

class Calibration {
public:
  double tagSize;
  double fx, fy;
  double px, py;

  Calibration(double tagSize, double fx, double fy, double px, double py) :
      tagSize(tagSize), fx(fx), fy(fy), px(px), py(py) {
  }

  const Eigen::Matrix<double, 3, 4> K() const {
    Eigen::Matrix<double, 3, 4> K;
    K.row(0) << fx, 0, px, 0;
    K.row(1) << 0, fy, py, 0;
    K.row(2) << 0, 0, 1, 0;
    return K;
  }

  friend std::ostream& operator<<(std::ostream& out, const Calibration& c) {
    out << "tagSize=" << c.tagSize << ", fx=" << c.fx << ", fy=" << c.fy
        << ", px=" << c.px << ", py=" << c.py << std::endl;
    return out;
  }
};

const Calibration calib_example(0.166, 570.0, 570.0, 320.0, 240.0);
