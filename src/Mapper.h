/**
 * @file Mapper.h
 * @brief Integrating Apriltags observations into a map using iSAM.
 * @author: Michael Kaess
 */

#pragma once

#include <map>
#include <vector>

#include <Eigen/Dense>

#include <AprilTags/TagDetector.h>

#include <isam/isam.h>

#include "Tags.h"
#include "tagSlam.h"

class Mapper {
  isam::Slam _slam;
  std::vector<isam::Pose3d_Node*> _poses;
  std::map<int32_t, isam::Pose3d_Node*> _targets;

  void add_pose(uint64_t utime);
  void add_observation(int32_t id, const isam::Pose3d& delta);
  void add_observation(int32_t id, const isam::Pose3d& delta,
      const TagCorners& tag, const Calibration& calibration);
  void update();

public:
  Mapper();

  void add_frame(uint64_t timestamp, const detections_t& detections,
      const Calibration& calibration);

  std::vector<isam::Pose3d> poses() const;

  std::map<int32_t, isam::Pose3d> targets() const;
};
