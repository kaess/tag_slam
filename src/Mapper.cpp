/**
 * @file Mapper.cpp
 * @brief Integrating Apriltags observations into a map using iSAM.
 * @author: Michael Kaess
 */

#include <Eigen/Dense>
#include <isam/isam.h>

#include "tagSlam.h"

#include "Mapper.h"

using namespace std;
using namespace Eigen;
using namespace isam;

// utility function
template<int base> Eigen::MatrixXd eye() {
  return Eigen::Matrix<double, base, base>::Identity();
}

Mapper::Mapper() {
  Properties prop;
  prop.mod_batch = 10;
  prop.method = DOG_LEG; // LEVENBERG_MARQUARDT;
  _slam.set_properties(prop);
}

void Mapper::add_pose(uint64_t utime) {
  Pose3d_Node* new_pose = new Pose3d_Node();
  _slam.add_node(new_pose);
  if (_poses.size() == 0) { // first node? then add prior
    Pose3d pose0;
    Noise noise = SqrtInformation(100. * eye<6>());
    Pose3d_Factor* prior = new Pose3d_Factor(new_pose, pose0, noise);
    _slam.add_factor(prior);
    _poses.push_back(new_pose);
  } else {
    // otherwise manually initialize (0 velocity model for now)
    Pose3d_Node* pose0 = _poses.back();
    new_pose->init(pose0->value());
    _poses.push_back(new_pose);
    // and add weak motion model
    Pose3d delta;
    Noise noise = SqrtInformation(100. * eye<6>()); // todo
    Pose3d_Pose3d_Factor* odo = new Pose3d_Pose3d_Factor(pose0, new_pose, delta,
        noise);
    _slam.add_factor(odo);
  }
}

void Mapper::add_observation(int32_t id, const Pose3d& delta) {
  if (_targets.find(id) == _targets.end()) {
    Pose3d_Node* new_target = new Pose3d_Node();
    _targets[id] = new_target;
    _slam.add_node(new_target);
  }
  Noise noise = SqrtInformation(100. * eye<6>()); // todo
  Pose3d_Node* pose = _poses.back();
  Pose3d_Node* target = _targets[id];
  Pose3d_Pose3d_Factor* factor = new Pose3d_Pose3d_Factor(pose, target, delta,
      noise);
  _slam.add_factor(factor);
}

void Mapper::add_observation(int32_t id, const Pose3d& delta,
    const TagCorners& tag, const Calibration& calib) {
  if (_targets.find(id) == _targets.end()) {
    Pose3d_Node* new_target = new Pose3d_Node();
    _targets[id] = new_target;
    _slam.add_node(new_target);
    // initialize target location based on single observation
    new_target->init(_poses.back()->value().oplus(delta));
  }
  Noise noise = SqrtInformation(10. * eye<8>()); // sigma: 1/10 pixel
  Pose3d_Node* pose = _poses.back();
  Pose3d_Node* target = _targets[id];
  Pose3d_April_Factor* factor = new Pose3d_April_Factor(pose, target, tag,
      noise, calib);
  _slam.add_factor(factor);
}

void Mapper::update() {
  _slam.update();
  //  _slam.batch_optimization();
}

void Mapper::add_frame(uint64_t timestamp, const detections_t& detections,
    const Calibration& calibration) {
  add_pose(timestamp);

  for (auto detection : detections) {
    Eigen::Matrix4d rot;
    rot.setZero();
    rot(1, 0) = 1;
    rot(2, 1) = 1;
    rot(0, 2) = 1;
    rot(3, 3) = 1;
    const Eigen::Matrix4d& T = detection.second;
    isam::Pose3d delta(rot * T);

    // add observed tag corners (delta is used for initialization)
    std::pair<float, float> p1 = detection.first.p[0];
    std::pair<float, float> p2 = detection.first.p[1];
    std::pair<float, float> p3 = detection.first.p[2];
    std::pair<float, float> p4 = detection.first.p[3];
    TagCorners corners(isam::Point2d(p1.first, p1.second),
        isam::Point2d(p2.first, p2.second), isam::Point2d(p3.first, p3.second),
        isam::Point2d(p4.first, p4.second));
    add_observation(detection.first.id, delta, corners, calibration);

  }

  update();
}

vector<Pose3d> Mapper::poses() const {
  vector<Pose3d> poses;
  for (auto node : _poses) {
    poses.push_back(node->value());
  }
  return poses;
}

map<int32_t, Pose3d> Mapper::targets() const {
  map<int32_t, Pose3d> targets;
  for (auto node : _targets) {
    targets[node.first] = node.second->value();
  }
  return targets;
}
