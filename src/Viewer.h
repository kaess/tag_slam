/**
 * @file Viewer.h
 * @brief Visualize Apriltags and camera trajectory.
 * @author: Michael Kaess
 */

#pragma once

#include <vector>

class Viewer {
public:

  Viewer();

  /**
   * GLUT initialization, open window.
   */
  void run();

  void update(const std::vector<isam::Pose3d> poses,
      const std::map<int32_t, isam::Pose3d> targets);

  /**
   * Signals processing thread to quit upon user request through GUI.
   */
  static bool exitRequested();

};
