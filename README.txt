
TagSlam: A simple SLAM example using Apriltags
Michael Kaess, 2014

external/ - Apriltags and iSAM libraries from external repositories
src/      - Header files for the iSAM library
bin/      - Executables (after calling "make")

Dependencies:
  - OpenCV
  - Eigen
  - SDL

In Ubuntu 12.04, install with:
  sudo apt-get install libopencv-dev libeigen3-dev libsdl1.2-dev
