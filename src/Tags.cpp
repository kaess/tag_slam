/**
 * @file Tags.cpp
 * @brief Apriltags interface including drawing
 * @author: Michael Kaess
 */

#include <string>
#include <chrono>

using namespace std;

#include <opencv2/opencv.hpp>

// April tags detector and various families that can be selected by command line option
#include <AprilTags/TagDetector.h>
#include <AprilTags/Tag16h5.h>
#include <AprilTags/Tag25h7.h>
#include <AprilTags/Tag25h9.h>
#include <AprilTags/Tag36h9.h>
#include <AprilTags/Tag36h11.h>

#include "Tags.h"

uint64_t tic() {
  chrono::system_clock::time_point now = chrono::system_clock::now();
  return chrono::duration_cast < chrono::nanoseconds
      > (now.time_since_epoch()).count();
}

const string _window = "Image Stream";

AprilTags::TagCodes Tags::tagCode(const string& code) {
  AprilTags::TagCodes tagCode(AprilTags::tagCodes36h11);
  if (code == "16h5") {
    tagCode = AprilTags::tagCodes16h5;
  } else if (code == "25h7") {
    tagCode = AprilTags::tagCodes25h7;
  } else if (code == "25h9") {
    tagCode = AprilTags::tagCodes25h9;
  } else if (code == "36h9") {
    tagCode = AprilTags::tagCodes36h9;
  } else if (code == "36h11") {
    tagCode = AprilTags::tagCodes36h11;
  } else {
    cout << "Invalid tag family, using default (36h11)" << endl;
  }
  return tagCode;
}

void Tags::markDetections(cv::Mat& image, const detections_t& detections) {
  for (auto detection : detections) {
    // draw corner points as detected by line intersection
    std::pair<float, float> p1 = detection.first.p[0];
    std::pair<float, float> p2 = detection.first.p[1];
    std::pair<float, float> p3 = detection.first.p[2];
    std::pair<float, float> p4 = detection.first.p[3];

    cv::line(image, cv::Point2f(p1.first, p1.second),
        cv::Point2f(p2.first, p2.second), cv::Scalar(255, 0, 0, 0));
    cv::line(image, cv::Point2f(p2.first, p2.second),
        cv::Point2f(p3.first, p3.second), cv::Scalar(0, 255, 0, 0));
    cv::line(image, cv::Point2f(p3.first, p3.second),
        cv::Point2f(p4.first, p4.second), cv::Scalar(0, 0, 255, 0));
    cv::line(image, cv::Point2f(p4.first, p4.second),
        cv::Point2f(p1.first, p1.second), cv::Scalar(255, 0, 255, 0));
    cv::circle(image,
        cv::Point2f(detection.first.cxy.first, detection.first.cxy.second), 8,
        cv::Scalar(0, 0, 255, 0), 2);

    cout << " " << detection.first.id << " hamming: "
        << detection.first.hammingDistance << endl;

    const Eigen::Matrix4d& T = detection.second;
    Eigen::Matrix<double, 3, 4> P = _calib.K() * T;

    Eigen::MatrixXd corners(4, 4);
    double s = _calib.tagSize / 2;
    corners << s, s, -s, -s, s, -s, -s, s, 0, 0, 0, 0, 1, 1, 1, 1;
    Eigen::Vector3d x;
    for (int i = 0; i < 4; i++) {
      x = P * corners.col(i);
      x = x / x(2);
      cv::circle(image, cv::Point2f(x(0), x(1)), 3, cv::Scalar(0, 255, 0, 0),
          1);
    }

    // print ID
    std::ostringstream strSt;
    strSt << "#" << detection.first.id;
    cv::putText(image, strSt.str(),
        cv::Point2f(detection.first.cxy.first + 10,
            detection.first.cxy.second + 10), cv::FONT_HERSHEY_PLAIN, 1,
        cv::Scalar(0, 0, 255));
  }
}

Tags::Tags(const string& code, const Calibration& calib) :
    _calib(calib), _tagDetector(AprilTags::TagDetector(tagCode(code))) {
  cv::namedWindow(_window);
}

Tags::~Tags() {
  cv::destroyWindow(_window);
}

detections_t Tags::detectTags(const cv::Mat& image) {
  cv::Mat imageGray;
  cvtColor(image, imageGray, CV_RGB2GRAY);
  std::vector<AprilTags::TagDetection> extracted = _tagDetector.extractTags(
      imageGray);

  detections_t detections;
  for (auto extraction : extracted) {
    Eigen::Matrix4d T = extraction.getRelativeTransform(_calib.tagSize,
        _calib.fx, _calib.fy, _calib.px, _calib.py);
    detections.push_back(make_pair(extraction, T));
  }

  return detections;
}

void Tags::showFrame(cv::Mat& image, uint64_t timestamp,
    const detections_t& detections) {
  static int num = 0;
  static uint64_t t0 = tic();

  markDetections(image, detections);

  cv::imshow(_window, image);

  num++;
  if (num % 30 == 0) {
    uint64_t t = tic();
    cout << 30. / (1e-9 * (double) (t - t0)) << " fps" << endl;
    t0 = t;
  }
}
