/**
 * @file Tags.h
 * @brief Apriltags interface including drawing
 * @author: Michael Kaess
 */

#pragma once

#include <string>

#include <Eigen/Dense>

#include <AprilTags/TagDetector.h>

#include "Calibration.h"

typedef std::vector<std::pair<AprilTags::TagDetection, Eigen::Matrix4d> > detections_t;

uint64_t tic();

class Tags {
  Calibration _calib;
  AprilTags::TagDetector _tagDetector;

  static AprilTags::TagCodes tagCode(const std::string& code);

  void markDetections(cv::Mat& image, const detections_t& detections);

public:

  Tags(const std::string& code, const Calibration& calib);

  ~Tags();

  detections_t detectTags(const cv::Mat& image);

  void showFrame(cv::Mat& image, uint64_t timestamp,
      const detections_t& detections);

};
